// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package com.gitlab.unibuild.example.maven.app.core;

import com.gitlab.unibuild.example.maven.dependency.SuperflousMathUtils;

public class MessageGenerator {

    public String generateMessage(final int a, final int b) {
        final int sum = SuperflousMathUtils.add(a, b);
        return String.format("%d + %d = %d", a, b, sum);
    }
}
