// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package com.gitlab.unibuild.example.maven.app;

import com.gitlab.unibuild.example.maven.app.core.MessageGenerator;
import com.gitlab.unibuild.example.maven.dependency.SuperflousMathUtils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class App {

    private static final MessageGenerator generator = new MessageGenerator();

    public static void main(final String[] args) {
        System.out.println(generator.generateMessage(2, 3));
    }

}